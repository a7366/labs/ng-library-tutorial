import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CounterButtonComponent} from './counter-button/counter-button.component';
import {MatBadgeModule} from "@angular/material/badge";
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [
    CounterButtonComponent
  ],
  imports: [
    CommonModule, MatBadgeModule, MatButtonModule
  ],
  exports: [CounterButtonComponent]
})
export class MyLibModule {
}
