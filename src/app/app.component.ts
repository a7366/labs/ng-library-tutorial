import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ngLibraryTutorial';
  currentCount = 0;

  handleCountChanged($event: number) {
    this.currentCount = $event;
  }
}
