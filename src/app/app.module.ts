import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MyLibModule} from "my-new-lib";

/**
 * une autre façon pour faire le refresh (app ↔ lib) consiste à travailler
 * directement avec la lib (pas via dist/) et de faire le build une fois que toute est ok
 * **/
// import {MyLibModule} from "../../projects/my-new-lib/src/lib/my-lib.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MyLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
